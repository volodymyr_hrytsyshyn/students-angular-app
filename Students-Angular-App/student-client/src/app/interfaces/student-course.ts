export interface StudentCourse {
  id: number;
  name: string;
  description: string;
  isSubscribed: boolean;
}
