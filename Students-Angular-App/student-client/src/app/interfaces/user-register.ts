export interface UserRegister {
  studentId: number;
  login: string;
  password: string;
}
