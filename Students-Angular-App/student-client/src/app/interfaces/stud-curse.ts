export interface StudCourse {
  id: number;
  name: string;
  description: string;
  isSubscribed: boolean;
}
