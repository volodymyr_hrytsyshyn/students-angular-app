export interface User {
  userId: number;
  studentId: number;
  login: string;
  role: string;
  token: string;
}
