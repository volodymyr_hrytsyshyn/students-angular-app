export interface AddCourse {
  name: string;
  description: string;
}
