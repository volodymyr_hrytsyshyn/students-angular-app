export interface AddStudentCourse {
  studentId: number;
  courseId: number;
}
