export interface Message {
        id: number;
        courseId: number;
        userId: number;
        login: string;
        text: string;
        date: Date;
}
